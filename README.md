This is sample application only to demonstrate how we can build web applcation using UI / UX Layer (View Layer)- Middleware / Services (Controller Layer) – Spring & ReST
Backend (Modal Layer) – MySQL

Following technologies being used:

Spring 4.3.0.RELEASE
Hibernate Core 4.3.10.Final
validation-api 1.1.0.Final
hibernate-validator 5.1.3.Final
MySQL Server 5.6
Maven 3
JDK 1.7
Tomcat 8.0.21
Eclipse Neon.3 Release (4.6.3)

You can build the WAR (either by eclipse) or via maven command line( mvn clean install). Deploy the war to a Servlet 3.0 container . 
Since i am using Tomcat, i will simply put this war file into tomcat webapps folder and click on start.bat inside tomcat/bin directory.

Open browser and browse at http://localhost:8080/CourseApplication/
